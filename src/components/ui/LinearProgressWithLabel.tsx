import { Box, LinearProgress, LinearProgressProps, Typography } from '@mui/material'
import { useMemo } from 'react'

interface LinearProgressWithLabelProps extends LinearProgressProps {
  value: number
}

const LinearProgressWithLabel: React.FC<LinearProgressWithLabelProps> = ({ value, ...props }) => {
  const valueToDisplay = useMemo(() => {
    if (typeof value === 'number') {
      return value
    } else {
      return 0
    }
  }, [value])

  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: '100%', mr: 1 }}>
        <LinearProgress variant="determinate" {...props} value={valueToDisplay} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="custom.slate.300">{`${Math.round(
          valueToDisplay
        )}%`}</Typography>
      </Box>
    </Box>
  )
}

export default LinearProgressWithLabel
