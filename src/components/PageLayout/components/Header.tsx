import { APP_ROUTES } from '@/routes/routes'
import { AppBar, Box, Button, Stack, Toolbar, Typography } from '@mui/material'
import { useNavigate } from 'react-router-dom'

export interface HeaderProps {
  title: string
  bodyNode?: React.ReactNode
}

export const Header: React.FC<HeaderProps> = ({ title }) => {
  const navigate = useNavigate()

  const handleAllTestsClick = () => {
    navigate(APP_ROUTES.TESTS.buildPath({}))
  }

  const handleResultsClick = () => {
    navigate(APP_ROUTES.TESTS.TEST_RESULTS.buildPath({}))
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="sticky" sx={{ backgroundColor: 'primary.dark' }} elevation={0}>
        <Toolbar
          sx={{
            color: 'primary.contrastText',
            justifyContent: {
              xs: 'center',
              sm: 'space-between',
            },
          }}
        >
          <Typography
            variant="h5"
            component="div"
            sx={{
              flexGrow: 1,
              fontWeight: 300,
              display: {
                xs: 'none',
                sm: 'block',
              },
            }}
          >
            {title}
          </Typography>

          <Stack direction="row" spacing={2}>
            <Button color="inherit" onClick={handleAllTestsClick}>
              Ваши тесты
            </Button>
            <Button color="inherit" onClick={handleResultsClick}>
              Результаты
            </Button>
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  )
}
