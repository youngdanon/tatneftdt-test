import { Box, Stack } from '@mui/material'
import { Header, HeaderProps } from './components/Header'

interface LayoutProps extends HeaderProps {
  children: React.ReactNode
}

export const PageLayout: React.FC<LayoutProps> = ({ children, title }) => {
  return (
    <Box
      sx={{
        backgroundColor: 'primary.dark',
        height: '100vh',
        overflowY: 'auto',
      }}
    >
      <Header title={title} />
      <Stack justifyContent="center" alignItems="center">
        <Box
          sx={{
            width: '100%',
            paddingX: {
              xs: '1rem',
              sm: '3rem',
              lg: '10rem',
              xl: '20rem',
              xxl: '30rem',
            },
          }}
        >
          {children}
        </Box>
      </Stack>
    </Box>
  )
}
