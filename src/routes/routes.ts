import { number, route } from 'react-router-typesafe-routes/dom'

export const APP_ROUTES = {
  TESTS: route(
    'tests/*',
    {},
    {
      SOLVE_TEST: route('solve/:id', {
        params: {
          id: number().defined(),
        },
      }),

      TEST_RESULTS: route(
        'results/*',
        {},
        {
          TEST_RESULTS_ID: route(':id', {
            params: {
              id: number().defined(),
            },
          }),
        }
      ),
    }
  ),
}
