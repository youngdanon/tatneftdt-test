import { Navigate, RouterProvider, createBrowserRouter } from 'react-router-dom'
import { APP_ROUTES } from './routes'
import TestsPage from '@/pages/tests'

export const RootRoutes = () => {
  return (
    <RouterProvider
      router={createBrowserRouter([
        { path: '/', element: <Navigate to={APP_ROUTES.TESTS.buildPath({})} /> },
        { path: APP_ROUTES.TESTS.relativePath, element: <TestsPage /> },
      ])}
    />
  )
}
