import { OverridesStyleRules } from '@mui/material/styles/overrides'

export const components: Partial<OverridesStyleRules> = {
  MuiButton: {
    styleOverrides: {
      root: {
        textTransform: 'none',
        boxShadow: 'none',
      },
      sizeSmall: {
        borderRadius: 8,
      },
      sizeMedium: {
        borderRadius: 10,
      },
      sizeLarge: {
        borderRadius: 12,
      },
    },
  },
  MuiChip: {
    styleOverrides: {
      root: {
        fontFamily: 'Montserrat',
      },
    },
  },
}
