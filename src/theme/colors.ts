const CUSTOM_COLORS = {
  slate: {
    50: '#f8fafc',
    100: '#f1f5f9',
    200: '#e2e8f0',
    300: '#cbd5e1',
    400: '#94a3b8',
    500: '#64748b',
    600: '#475569',
    700: '#334155',
    800: '#1e293b',
    900: '#0f172a',
  },
} as const

export const COLORS = {
  custom: CUSTOM_COLORS,
  primary: {
    main: '#3d2c55',
    dark: '#141634',
    light: '#f9f3d1',
    contrastText: 'white',
  },
  secondary: {
    main: '#cf9fa1',
    contrastText: '#382a2b',
  },
  error: {
    main: '#f44336',
  },
  warning: {
    main: '#decc38',
  },
  info: {
    main: '#bd539d',
  },
  success: {
    main: '#38a16b',
  },
  text: {
    primary: '#3d2c55',
    secondary: '#cf9fa1',
  },
}
