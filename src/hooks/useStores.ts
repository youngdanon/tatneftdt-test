import storeContext from '@/contexts/StoreContext'
import { useContext } from 'react'

/** Хук для доступа к хранилищам */
export const useStores = () => useContext(storeContext)
