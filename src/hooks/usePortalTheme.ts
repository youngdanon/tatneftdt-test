import { COLORS } from '@/theme/colors'
import { components } from '@/theme/components'
import { typography } from '@/theme/typography'
import { createTheme } from '@mui/material'

export const usePortalTheme = () => {
  return createTheme({
    typography,
    components,
    palette: COLORS,
  })
}
