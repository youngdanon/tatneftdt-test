import { PageLayout } from '@/components/PageLayout'
import { useStores } from '@/hooks/useStores'
import { APP_ROUTES } from '@/routes/routes'
import { observer } from 'mobx-react-lite'
import { useEffect, useState } from 'react'
import { useTypedParams } from 'react-router-typesafe-routes/dom'
import { TestResultHeader } from './components/TestResultHeader'
import { TestResultTable } from './components/TestResultTable'
import { TestStartDialog } from '../../listView/common/TestStartDialog'
import { useNavigate } from 'react-router-dom'

/** Страница результатов теста */
export const TestResultView: React.FC = observer(() => {
  const { id } = useTypedParams(APP_ROUTES.TESTS.TEST_RESULTS.TEST_RESULTS_ID)
  const { testResultStore, testStore } = useStores()
  const navigate = useNavigate()
  /** Открыто ли диалоговое окно запуска теста */
  const [isTestStartDialogOpen, setIsTestStartDialogOpen] = useState(false)

  useEffect(() => {
    testResultStore.fetch(id)
    return () => {
      testResultStore.clearStore()
    }
  }, [])

  const handleCloseTestStartDialog = () => {
    setIsTestStartDialogOpen(false)
  }

  const handleGoToTest = () => {
    testStore.clearStore()
    setIsTestStartDialogOpen(false)
    navigate(APP_ROUTES.TESTS.SOLVE_TEST.buildPath({ id: id }))
  }

  return (
    <>
      <PageLayout title="Результаты теста">
        <TestResultHeader
          testName={testResultStore.data?.data.name ?? ''}
          correctAnswersCount={testResultStore.correctAnswersCount}
          totalQuestions={testResultStore.totalQuestionsCount}
          userScore={testResultStore.userScore}
          maxScore={testResultStore.maxScore}
          timeSpent={testResultStore.timeSpent}
          userName={testResultStore.userName}
          passedAt={testResultStore.passedAt}
          onRetakeTestClick={() => setIsTestStartDialogOpen(true)}
        />
        <TestResultTable
          sx={{
            my: 2,
          }}
        />
      </PageLayout>
      <TestStartDialog
        isOpen={isTestStartDialogOpen}
        onClose={handleCloseTestStartDialog}
        onConfirm={handleGoToTest}
      />
    </>
  )
})
