import { useStores } from '@/hooks/useStores'
import {
  Card,
  Chip,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableContainerProps,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import { observer } from 'mobx-react-lite'
import { getAnswerColor } from '../../solveTest/components/Question/utils'

interface TestResultTableProps {
  sx?: TableContainerProps['sx']
}

/** Компонент для отображения таблицы результатов теста */
export const TestResultTable: React.FC<TestResultTableProps> = observer(({ sx }) => {
  const { testResultStore } = useStores()

  return (
    <TableContainer
      component={Card}
      sx={{
        width: '100%',
        ...sx,
      }}
    >
      <Table sx={{ width: '100%' }}>
        <TableHead>
          <TableRow>
            <TableCell>Вопрос</TableCell>
            <TableCell>Ваш ответ</TableCell>
            <TableCell>Правильный ответ</TableCell>
            <TableCell>Баллы</TableCell>
            <TableCell>Статус</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {testResultStore.data?.data.questions.map((question) => (
            <TableRow key={question.id}>
              <TableCell>{question.question}</TableCell>
              <TableCell>
                {testResultStore.getUserAnswers(question).map((answer) => (
                  <Typography
                    variant="body2"
                    color={getAnswerColor(answer, true, true)}
                    key={answer.id}
                  >
                    {answer.answer}
                  </Typography>
                ))}
              </TableCell>
              <TableCell>
                {testResultStore.getCorrectAnswers(question).map((answer) => (
                  <Typography variant="body2" color="success" key={answer.id}>
                    {answer.answer}
                  </Typography>
                ))}
              </TableCell>
              <TableCell>{question.userScore.toFixed(2).replace('.00', '')}</TableCell>
              <TableCell>
                {question.isUserAnsweredCorrectly ? (
                  <Chip color="success" label="Правильно" />
                ) : (
                  <Chip color="error" label="Неправильно" />
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
})
