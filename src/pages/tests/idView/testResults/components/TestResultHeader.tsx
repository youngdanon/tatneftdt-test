import LinearProgressWithLabel from '@/components/ui/LinearProgressWithLabel'
import { Box, Button, Divider, Stack, Typography } from '@mui/material'
import { useMemo } from 'react'

interface TestResultHeaderProps {
  /** Название теста */
  testName: string
  /** Количество правильных ответов */
  correctAnswersCount: number
  /** Общее количество вопросов */
  totalQuestions: number
  /** Количество баллов пользователя */
  userScore: string
  /** Максимальное количество баллов */
  maxScore: number
  /** Время, потраченное на прохождение теста */
  timeSpent: string
  /** Имя пользователя */
  userName: string
  /** Дата прохождения теста */
  passedAt: string
  /** Обработчик нажатия на кнопку "Пройти тест заново" */
  onRetakeTestClick?: () => void
}

/** Шапка страницы результатов теста */
export const TestResultHeader: React.FC<TestResultHeaderProps> = ({
  testName,
  correctAnswersCount,
  totalQuestions,
  userScore,
  maxScore,
  timeSpent,
  userName,
  passedAt,
  onRetakeTestClick,
}) => {
  const testResultPercent = useMemo(
    () => (+userScore / maxScore) * 100,
    [correctAnswersCount, totalQuestions]
  )

  const testResultColor = useMemo(() => {
    if (testResultPercent < 50) {
      return 'error'
    } else if (testResultPercent < 75) {
      return 'warning'
    } else {
      return 'success'
    }
  }, [testResultPercent])

  return (
    <Stack spacing={3}>
      <Typography variant="body2" color="white">
        ФИО проходившего: <b>{userName}</b>
      </Typography>
      <Stack spacing={2}>
        <Box
          sx={{
            width: {
              xs: '100%',
              sm: '50%',
            },
          }}
        >
          <LinearProgressWithLabel value={testResultPercent} color={testResultColor} />
        </Box>
        <Stack
          direction={{
            sm: 'column',
            lg: 'row',
          }}
          spacing={2}
        >
          <Typography variant="body2" color="white" fontWeight="500">
            {correctAnswersCount} из {totalQuestions} правильных ответов
          </Typography>
          <Divider
            orientation="vertical"
            flexItem
            sx={{
              borderColor: 'white',
            }}
          />
          <Typography variant="body2" color="white" fontWeight="500">
            {userScore} из {maxScore} баллов
          </Typography>
          <Divider
            orientation="vertical"
            flexItem
            sx={{
              borderColor: 'white',
            }}
          />
          <Typography variant="body2" color="white" fontWeight="500">
            Затрачено времени: {timeSpent}
          </Typography>
          <Divider
            orientation="vertical"
            flexItem
            sx={{
              borderColor: 'white',
            }}
          />
          <Typography variant="body2" color="white" fontWeight="500">
            Пройдено: {new Date(passedAt).toLocaleDateString()}
          </Typography>
        </Stack>
      </Stack>
      <Stack direction="row" spacing={2}>
        <Typography variant="h6" color="white" fontWeight="500">
          {testName}
        </Typography>
        <Button color="secondary" variant="contained" onClick={onRetakeTestClick}>
          Пройти тест заново
        </Button>
      </Stack>
    </Stack>
  )
}
