import { COLORS } from '@/theme/colors'
import { Answer } from '@/types/Test'

/** Получить цвет ответа
 * @param answer Ответ
 * @param isUserAnswered Ответил ли пользователь на вопрос
 * @param isDefault Вернуть ли дефолтный цвет
 */
export function getAnswerColor(
  answer: Answer,
  isUserAnswered: boolean,
  isDefault: true
): 'success' | 'error' | undefined
export function getAnswerColor(
  answer: Answer,
  isUserAnswered: boolean,
  isDefault: false
): string | undefined
export function getAnswerColor(answer: Answer, isUserAnswered: boolean, isDefault: boolean) {
  if (isUserAnswered) {
    if (answer.isCorrect) {
      return isDefault ? 'success' : COLORS.success.main
    } else {
      return isDefault ? 'error' : COLORS.error.main
    }
  }
}
