import { useStores } from '@/hooks/useStores'
import { FormControl, FormControlLabel, Radio, RadioGroup } from '@mui/material'
import { observer } from 'mobx-react-lite'
import { TopLabel } from './TopLabel'
import { getAnswerColor } from './utils'
import { runInAction } from 'mobx'

/** Компонент для отображения вопроса с одиночным выбором */
export const SingleAnswerQuestion: React.FC = observer(() => {
  const { testStore } = useStores()

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (testStore.currentQuestion?.isUserAnswered) return
    runInAction(() => {
      testStore.currentQuestion?.setUserAnswers([+event.target.value])
    })
  }

  return (
    <FormControl>
      <TopLabel />
      <RadioGroup onChange={handleChange} value={testStore.currentQuestion?.userAnswers[0] ?? null}>
        {testStore.currentQuestion?.shuffledAnswers.map((answer) => (
          <FormControlLabel
            key={answer.id}
            value={answer.id}
            control={
              <Radio
                sx={{
                  color: getAnswerColor(answer, !!testStore.currentQuestion?.isUserAnswered, false),
                  '&.Mui-checked': {
                    color: getAnswerColor(
                      answer,
                      !!testStore.currentQuestion?.isUserAnswered,
                      false
                    ),
                  },
                }}
              />
            }
            label={answer.answer}
            slotProps={{
              typography: {
                color: getAnswerColor(answer, !!testStore.currentQuestion?.isUserAnswered, false),
              },
            }}
          />
        ))}
      </RadioGroup>
    </FormControl>
  )
})
