import { useStores } from '@/hooks/useStores'
import { FormLabel } from '@mui/material'

/** Компонент для отображения верхней подписи вопроса */
export const TopLabel: React.FC = () => {
  const { testStore } = useStores()

  return (
    <FormLabel
      sx={{
        color: testStore.currentQuestion?.questionHintColor,
        '&.Mui-focused': {
          color: testStore.currentQuestion?.questionHintColor,
        },
        fontWeight: testStore.currentQuestion?.isUserAnswered ? '500' : '400',
      }}
    >
      {testStore.currentQuestion?.questionHint}
    </FormLabel>
  )
}
