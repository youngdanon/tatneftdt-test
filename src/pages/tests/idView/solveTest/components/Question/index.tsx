import { useStores } from '@/hooks/useStores'
import { COLORS } from '@/theme/colors'
import { QuestionType } from '@/types/Test'
import { Button, Card, Stack } from '@mui/material'
import { observer } from 'mobx-react-lite'
import { useMemo } from 'react'
import { SingleAnswerQuestion } from './SingleAnswerQuestion'
import { MultipleAnswerQuestion } from './MultipleAnswerQuestion'
import { useNavigate } from 'react-router-dom'
import { APP_ROUTES } from '@/routes/routes'

/** Карточка вопроса */
export const QuestionCard: React.FC = observer(() => {
  const { testStore } = useStores()
  const navigate = useNavigate()

  const cardContent = useMemo(() => {
    switch (testStore.currentQuestion?.type) {
      case QuestionType.Single:
        return <SingleAnswerQuestion />
      case QuestionType.Multiple:
        return <MultipleAnswerQuestion />
      default:
        return null
    }
  }, [testStore.currentQuestion?.type])

  const handleAnswer = () => {
    testStore.currentQuestion?.answerQuestion()
  }

  const handleNextQuestion = () => {
    testStore.nextQuestion()
  }

  const handleFinishTest = () => {
    testStore.finishTest()
    navigate(APP_ROUTES.TESTS.TEST_RESULTS.TEST_RESULTS_ID.buildPath({ id: testStore.id }))
  }

  return (
    <Card
      sx={{
        px: '2rem',
        py: '2rem',
        borderRadius: '1rem',
        border: `1px solid ${COLORS.custom.slate[200]}`,
      }}
      elevation={0}
    >
      <Stack spacing={2}>
        {cardContent}
        <Stack direction="row" spacing={2} justifyContent="flex-end">
          {!testStore.currentQuestion?.isUserAnswered ? (
            <Button
              variant="contained"
              color="primary"
              disabled={!testStore.currentQuestion?.canUserAnswer}
              onClick={handleAnswer}
            >
              Ответить
            </Button>
          ) : (
            <>
              {testStore.isCurrentQuestionLast ? (
                <Button variant="contained" color="primary" onClick={handleFinishTest}>
                  Завершить тест
                </Button>
              ) : (
                <Button variant="contained" color="primary" onClick={handleNextQuestion}>
                  Следующий вопрос
                </Button>
              )}
            </>
          )}
        </Stack>
      </Stack>
    </Card>
  )
})
