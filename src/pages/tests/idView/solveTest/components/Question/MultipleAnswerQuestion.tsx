import { useStores } from '@/hooks/useStores'
import { Checkbox, FormControl, FormControlLabel, FormGroup } from '@mui/material'
import { entries, observable, runInAction, set, values, keys, remove } from 'mobx'
import { observer } from 'mobx-react-lite'
import { useEffect } from 'react'
import { getAnswerColor } from './utils'
import { TopLabel } from './TopLabel'

/** Хранилище для хранения выбранных ответов */
const checkedValues = observable.object<{ [answerId: number]: boolean }>({})
/** Функция для очистки выбранных ответов */
const clearCheckedValues = () => {
  const checkedKeys = keys(checkedValues).map((key) => +(key as unknown as string))
  runInAction(() => {
    checkedKeys.forEach((key) => {
      // @ts-expect-error key is number
      remove(checkedValues, key)
    })
  })
}

/** Компонент для отображения вопроса с множественным выбором */
export const MultipleAnswerQuestion: React.FC = observer(() => {
  const { testStore } = useStores()

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, answerId: number) => {
    if (testStore.currentQuestion?.isUserAnswered) return
    runInAction(() => {
      set(checkedValues, { [answerId]: event.target.checked })
    })
  }

  useEffect(() => {
    clearCheckedValues()
  }, [testStore.currentQuestionIndex])

  useEffect(() => {
    /** Сохраняем выбранные ответы в стор теста */
    const userAnswers = entries(checkedValues)
      .filter(([, value]) => value)
      .map(([key]) => +key)
    testStore.currentQuestion?.setUserAnswers(userAnswers)
  }, [values(checkedValues)])

  return (
    <FormControl>
      <TopLabel />
      <FormGroup>
        {testStore.currentQuestion?.shuffledAnswers.map((answer) => (
          <FormControlLabel
            key={answer.id}
            control={
              <Checkbox
                checked={checkedValues[answer.id] ?? false}
                onChange={(e) => {
                  handleChange(e, answer.id)
                }}
                color={getAnswerColor(answer, !!testStore.currentQuestion?.isUserAnswered, true)}
              />
            }
            slotProps={{
              typography: {
                color: getAnswerColor(answer, !!testStore.currentQuestion?.isUserAnswered, false),
              },
            }}
            label={answer.answer}
          />
        ))}
      </FormGroup>
    </FormControl>
  )
})
