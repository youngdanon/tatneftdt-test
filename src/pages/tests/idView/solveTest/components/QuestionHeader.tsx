import LinearProgressWithLabel from '@/components/ui/LinearProgressWithLabel'
import { Stack, Typography } from '@mui/material'
import { useMemo } from 'react'

interface QuestionHeaderProps {
  /** Название вопроса */
  questionName: string
  /** Номер текущего вопроса */
  currentQuestionIndex: number
  /** Количество вопросов, которые прошел пользователь */
  userPassedQuestionsCount: number
  /** Общее количество вопросов */
  totalQuestions: number
}

/** Шапка страницы решения теста */
export const QuestionHeader: React.FC<QuestionHeaderProps> = ({
  questionName,
  currentQuestionIndex,
  userPassedQuestionsCount,
  totalQuestions,
}) => {
  const progress = useMemo(
    () => (userPassedQuestionsCount / totalQuestions) * 100,
    [userPassedQuestionsCount, totalQuestions]
  )

  const progressColor = useMemo(() => {
    if (progress < 50) {
      return 'error'
    } else if (progress < 75) {
      return 'warning'
    } else {
      return 'success'
    }
  }, [progress])

  return (
    <Stack spacing={2}>
      <Stack
        spacing={2}
        sx={{
          width: {
            xs: '100%',
            sm: '50%',
          },
        }}
      >
        <LinearProgressWithLabel value={progress} color={progressColor} />
        <Typography variant="body2" color="white" fontWeight="500">
          Вопрос {currentQuestionIndex} из {totalQuestions}
        </Typography>
      </Stack>
      <Typography variant="h6" color="white" fontWeight="500">
        {questionName}
      </Typography>
    </Stack>
  )
}
