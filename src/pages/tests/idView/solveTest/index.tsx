import { PageLayout } from '@/components/PageLayout'
import { useStores } from '@/hooks/useStores'
import { APP_ROUTES } from '@/routes/routes'
import { observer } from 'mobx-react-lite'
import { useEffect } from 'react'
import { useTypedParams } from 'react-router-typesafe-routes/dom'
import { QuestionHeader } from './components/QuestionHeader'
import Stack from '@mui/material/Stack'
import { QuestionCard } from './components/Question'

/** Страница решения теста */
export const TestMain: React.FC = observer(() => {
  const { testStore } = useStores()
  const { id } = useTypedParams(APP_ROUTES.TESTS.SOLVE_TEST)

  useEffect(() => {
    testStore.fetch(id)
    testStore.startTest()
    return () => {
      testStore.clearStore()
    }
  }, [id])

  return (
    <PageLayout title="Решение теста">
      <Stack spacing={2}>
        <QuestionHeader
          questionName={testStore.currentQuestion?.questionName ?? ''}
          userPassedQuestionsCount={testStore.userPassedQuestionsCount}
          currentQuestionIndex={testStore.currentQuestionIndex + 1}
          totalQuestions={testStore.totalQuestionsCount}
        />
        <QuestionCard />
      </Stack>
    </PageLayout>
  )
})
