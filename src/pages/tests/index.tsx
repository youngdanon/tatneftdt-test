import { Route, Routes } from 'react-router-dom'
import TestsListView from './listView/testsList'
import { APP_ROUTES } from '@/routes/routes'
import { TestMain, TestResultView } from './idView'
import { TestResultsView } from './listView/resultsList'

const TestsPage: React.FC = () => {
  return (
    <Routes>
      <Route index element={<TestsListView />} />
      <Route path={APP_ROUTES.TESTS.$.SOLVE_TEST.relativePath} element={<TestMain />} />
      <Route path={APP_ROUTES.TESTS.$.TEST_RESULTS.relativePath} element={<TestResultsView />} />
      <Route
        path={APP_ROUTES.TESTS.$.TEST_RESULTS.TEST_RESULTS_ID.relativePath}
        element={<TestResultView />}
      />
    </Routes>
  )
}

export default TestsPage
