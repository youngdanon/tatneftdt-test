import { PageLayout } from '@/components/PageLayout'
import { useStores } from '@/hooks/useStores'
import { Stack, Typography } from '@mui/material'
import { observer } from 'mobx-react-lite'
import { useEffect, useState } from 'react'
import { TestCard } from './components/TestCard'
import { Test } from '@/types/Test'
import { useNavigate } from 'react-router-dom'
import { APP_ROUTES } from '@/routes/routes'
import { TestStartDialog } from '../common/TestStartDialog'

/** Страница со списком тестов */
const TestsListView: React.FC = observer(() => {
  const { testsListStore } = useStores()
  const navigate = useNavigate()

  /** Открыто ли диалоговое окно запуска теста */
  const [isTestStartDialogOpen, setIsTestStartDialogOpen] = useState(false)
  /** Выбранный тест */
  const [selectedTest, setSelectedTest] = useState<Test | null>(null)

  useEffect(() => {
    testsListStore.fetchTests()
    testsListStore.fetchSolvedTestsIds()
    return () => {
      testsListStore.clearStore()
    }
  }, [])

  const handleTestClick = (test: Test, isTestSolved: boolean) => {
    setSelectedTest(test)
    if (isTestSolved) {
      navigate(APP_ROUTES.TESTS.TEST_RESULTS.TEST_RESULTS_ID.buildPath({ id: test.id }))
      return
    }
    setIsTestStartDialogOpen(true)
  }

  const handleCloseTestStartDialog = () => {
    setIsTestStartDialogOpen(false)
  }

  const handleGoToTest = () => {
    setIsTestStartDialogOpen(false)
    navigate(APP_ROUTES.TESTS.SOLVE_TEST.buildPath({ id: selectedTest?.id ?? 0 }))
  }

  return (
    <>
      <PageLayout title="Ваши тесты">
        <Typography variant="h3" color="white" fontWeight="500">
          Ваши тесты
        </Typography>
        <Stack spacing={2} sx={{ mt: 4 }}>
          {testsListStore.tests.map((test) => (
            <TestCard
              key={test.id}
              test={test}
              onClick={() => handleTestClick(test, testsListStore.solvedTestsIds.includes(test.id))}
              isTestSolved={testsListStore.solvedTestsIds.includes(test.id)}
            />
          ))}
        </Stack>
      </PageLayout>
      <TestStartDialog
        isOpen={isTestStartDialogOpen}
        onClose={handleCloseTestStartDialog}
        onConfirm={handleGoToTest}
      />
    </>
  )
})

export default TestsListView
