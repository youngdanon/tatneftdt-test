import { QuestionType, Test } from '@/types/Test'
import { Card, CardProps, Chip, Stack, Typography } from '@mui/material'
import OpenInNewIcon from '@mui/icons-material/OpenInNew'
import { COLORS } from '@/theme/colors'
import { useMemo } from 'react'

interface TestCardProps {
  test: Test
  onClick?: (test: Test) => void
  isTestSolved: boolean
  sx?: CardProps['sx']
}

export const TestCard: React.FC<TestCardProps> = ({
  test,
  onClick: onTestClick,
  sx,
  isTestSolved,
}) => {
  /** Пример вопроса из теста */
  const sampleQuestion = useMemo(() => {
    const question = test.questions[0]?.question
    if (!question) return 'Вопросов нет'
    return question.endsWith('?') ? question : question + '?'
  }, [test.questions.length])

  /** Типы вопросов в тесте */
  const questionTypes = useMemo(() => {
    const types = new Set<QuestionType>()
    test.questions.forEach((question) => {
      types.add(question.type)
    })
    return types
  }, [test.questions.length])

  const border = useMemo(() => {
    if (isTestSolved) {
      return `2px solid ${COLORS.success.main}`
    } else {
      return `1px solid ${COLORS.custom.slate[200]}`
    }
  }, [isTestSolved])

  return (
    <Card
      sx={{
        p: 2,
        cursor: 'pointer',
        position: 'relative',
        borderRadius: '1rem',
        border,
        ...sx,
      }}
      elevation={0}
      onClick={() => onTestClick?.(test)}
    >
      <Stack spacing={2}>
        <Stack direction="row" spacing={1} alignItems="center">
          <Typography variant="h5" fontWeight="500" color="primary.dark">
            {test.name}
          </Typography>
          <OpenInNewIcon sx={{ color: COLORS.custom.slate[400] }} />
          {isTestSolved && <Chip size="small" label="Решен" color="success" />}
        </Stack>
        <Stack direction="row" justifyContent="space-between">
          <Typography variant="subtitle1" color="custom.slate.400">
            {test.questions.length} вопросов
          </Typography>
          <Typography variant="subtitle1" color="custom.slate.400">
            {questionTypes.size} типа вопросов
          </Typography>
        </Stack>
      </Stack>
      <Typography
        variant="subtitle1"
        color="custom.slate.200"
        fontWeight="500"
        fontSize="2.5rem"
        sx={{
          position: 'absolute',
          top: '-1.2rem',
          right: '-0.5rem',
          display: {
            xs: 'none',
            lg: 'block',
          },
        }}
      >
        {sampleQuestion}
      </Typography>
    </Card>
  )
}
