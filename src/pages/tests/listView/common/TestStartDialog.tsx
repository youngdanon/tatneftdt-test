import { useStores } from '@/hooks/useStores'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
} from '@mui/material'
import { observer } from 'mobx-react-lite'

interface Props {
  /** Открыто ли диалоговое окно */
  isOpen: boolean
  /** Обработчик закрытия диалогового окна */
  onClose: () => void
  /** Обработчик нажатия на кнопку "Начать тест" */
  onConfirm: () => void
}

/** Диалоговое окно запуска теста */
export const TestStartDialog: React.FC<Props> = observer(({ isOpen, onClose, onConfirm }) => {
  const { testStore } = useStores()

  const isConfirmButtonDisabled = !testStore.userName

  const handleClose = () => {
    onClose()
  }

  const handleConfirm = () => {
    onConfirm()
  }

  return (
    <Dialog open={isOpen} onClose={onClose}>
      <DialogTitle>Запуск теста</DialogTitle>
      <DialogContent>
        <DialogContentText>Введите ваше ФИО и нажмите кнопку "Начать тест"</DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          label="ФИО"
          fullWidth
          variant="standard"
          value={testStore.userName}
          onChange={(e) => testStore.setUserName(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Отмена</Button>
        <Button onClick={handleConfirm} disabled={isConfirmButtonDisabled}>
          Начать тест
        </Button>
      </DialogActions>
    </Dialog>
  )
})
