import LinearProgressWithLabel from '@/components/ui/LinearProgressWithLabel'
import { COLORS } from '@/theme/colors'
import { TestResult } from '@/types/TestResults'
import { Card, Stack, Typography } from '@mui/material'
import { useMemo } from 'react'

export interface TestResultCardProps {
  testResult: TestResult
  onClick?: (testResult: TestResult) => void
}

export const TestResultCard: React.FC<TestResultCardProps> = ({ testResult, onClick }) => {
  const testResultPercent = useMemo(
    () => (+testResult.data.userScore / testResult.data.maxScore) * 100,
    [testResult.data.userScore, testResult.data.maxScore]
  )

  const testResultColor = useMemo(() => {
    if (testResultPercent < 50) {
      return 'error'
    } else if (testResultPercent < 75) {
      return 'warning'
    } else {
      return 'success'
    }
  }, [testResultPercent])

  const correctAnswersCount = useMemo(() => {
    return testResult.data.questions.filter((question) => question.isUserAnsweredCorrectly).length
  }, [testResult.data.questions.length])

  const userScore = useMemo(() => {
    return testResult.data.userScore?.toFixed(2).replace('.00', '') || '0'
  }, [testResult.data.userScore])

  const timeSpent = useMemo(() => {
    const startDate = new Date(testResult.testStart)
    const endDate = new Date(testResult.testEnd)
    const delta = new Date(endDate.getTime() - startDate.getTime())

    return `${delta.getMinutes()} минут ${delta.getSeconds()} секунд`
  }, [testResult.testStart, testResult.testEnd])

  return (
    <Card
      sx={{
        p: 2,
        cursor: 'pointer',
        position: 'relative',
        borderRadius: '1rem',
      }}
      elevation={0}
      onClick={() => onClick?.(testResult)}
    >
      <Stack spacing={3}>
        <Stack spacing={1}>
          <Stack direction="row" spacing={1} justifyContent="space-between">
            <Typography variant="h5" fontWeight="500">
              {testResult.data.name}
            </Typography>
            <Typography variant="body2" color="custom.slate.400" textAlign="right">
              Пройдено: <b>{new Date(testResult.testEnd).toLocaleDateString()}</b>
            </Typography>
          </Stack>
          <Typography variant="body2">
            ФИО проходившего: <b>{testResult.data.userName}</b>
          </Typography>
        </Stack>

        <Stack spacing={1}>
          <LinearProgressWithLabel value={testResultPercent} color={testResultColor} />
          <Stack
            direction={{ xs: 'column', sm: 'row' }}
            justifyContent="space-between"
            sx={{
              p: 1,
              borderRadius: '0.3rem',
              border: `1px solid ${COLORS.custom.slate[200]}`,
            }}
            flexWrap="wrap"
          >
            <Typography variant="body2" fontWeight="500" mx="auto">
              {correctAnswersCount} из {testResult.data.questions.length} правильных ответов
            </Typography>
            <Typography variant="body2" fontWeight="500" mx="auto">
              {userScore} из {testResult.data.maxScore} баллов
            </Typography>
            <Typography variant="body2" fontWeight="500" mx="auto">
              Затрачено времени: {timeSpent}
            </Typography>
          </Stack>
        </Stack>
      </Stack>
    </Card>
  )
}
