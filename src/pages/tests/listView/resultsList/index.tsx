import { PageLayout } from '@/components/PageLayout'
import { useStores } from '@/hooks/useStores'
import { Stack, Typography } from '@mui/material'
import { observer } from 'mobx-react-lite'
import { useEffect } from 'react'
import { TestResultCard } from './components/TestResultCard'
import { useNavigate } from 'react-router-dom'
import { APP_ROUTES } from '@/routes/routes'

/** Страница со списком результатов тестов */
export const TestResultsView: React.FC = observer(() => {
  const { testResultsListStore } = useStores()
  const navigate = useNavigate()

  useEffect(() => {
    testResultsListStore.fetchTestResults()
    return () => {
      testResultsListStore.clearStore()
    }
  }, [])

  const handleTestResultClick = (testId: number) => {
    navigate(APP_ROUTES.TESTS.TEST_RESULTS.TEST_RESULTS_ID.buildPath({ id: testId }))
  }

  return (
    <PageLayout title="Результаты тестов">
      <Typography variant="h3" color="white" fontWeight="500">
        Результаты прохождения тестов
      </Typography>
      <Stack spacing={2} sx={{ mt: 4 }}>
        {testResultsListStore.testResults.map((testResult) => (
          <TestResultCard
            key={testResult.id}
            testResult={testResult}
            onClick={(test) => handleTestResultClick(test.data.id)}
          />
        ))}
      </Stack>
    </PageLayout>
  )
})
