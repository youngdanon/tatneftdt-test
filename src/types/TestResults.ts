import { TestStoreExportType } from '@/stores/TestStore/types'

export type TestResult = {
  id: number
  /** Время начала теста */
  testStart: string
  /** Время окончания теста */
  testEnd: string
  /** Данные теста */
  data: TestStoreExportType
}
