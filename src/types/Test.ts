export type Answer = {
  id: number
  /** Текст ответа */
  answer: string
  /** Правильный ли ответ */
  isCorrect: boolean
}

export enum QuestionType {
  /** Вопрос с одним правильным ответом */
  Single = 'single',
  /** Вопрос с несколькими правильными ответами */
  Multiple = 'multiple',
}

export const QuestionTypeNames: { [key in QuestionType]: string } = {
  [QuestionType.Single]: 'Один ответ',
  [QuestionType.Multiple]: 'Несколько ответов',
}
export type Question = {
  id: number
  /** Текст вопроса */
  question: string
  /** Тип вопроса */
  type: QuestionType
  /** Ответы на вопрос */
  answers: Answer[]
}

export type ScoresByTypes = { [key in QuestionType]: number }

export type Test = {
  id: number
  /** Название теста */
  name: string
  /** Баллы за различные типы вопросов */
  scoresByTypes: ScoresByTypes
  /** Вопросы теста */
  questions: Question[]
}
