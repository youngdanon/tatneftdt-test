import TestResultStore from '@/stores/TestResultStore'
import TestResultsListStore from '@/stores/TestResultsListStore'
import TestStore from '@/stores/TestStore'
import TestsListStore from '@/stores/TestsListStore'
import { createContext } from 'react'

const storeContext = createContext({
  testsListStore: new TestsListStore(),
  testStore: new TestStore(),
  testResultStore: new TestResultStore(),
  testResultsListStore: new TestResultsListStore(),
})

export default storeContext
