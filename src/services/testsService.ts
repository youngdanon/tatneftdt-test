import { Test } from '@/types/Test'
import tests from '@/data/tests.json'
import { TestResult } from '@/types/TestResults'
import { SaveTestResultsDto } from './types'

const sampleDelay = 0

/** Сервис для работы с тестами */
class TestService {
  /** Получить список тестов */
  static async getTestsList(): Promise<Test[]> {
    return new Promise<Test[]>((resolve) => {
      setTimeout(() => {
        resolve(tests as Test[])
      }, sampleDelay)
    })
  }

  /** Получить тест по id */
  static async getTestById(id: number): Promise<Test> {
    return new Promise<Test>((resolve, reject) => {
      setTimeout(() => {
        const test = tests.find((test) => test.id === id)
        if (test) {
          resolve(test as Test)
        } else {
          reject(new Error('Test not found'))
        }
      }, sampleDelay)
    })
  }

  /** Поиск тестов по подстроке */
  static async findTestsByName(name: string): Promise<Test[]> {
    return new Promise<Test[]>((resolve) => {
      setTimeout(() => {
        const filteredTests = tests.filter((test) => test.name.includes(name))
        resolve(filteredTests as Test[])
      }, sampleDelay)
    })
  }

  /** Сохранить результаты теста */
  static saveTestResults(testResult: SaveTestResultsDto): TestResult[] {
    const testResults = localStorage.getItem('testResults')
    const tests: TestResult[] = testResults ? JSON.parse(testResults) : []
    const newTests = tests.filter((test) => test.data.id !== testResult.data.id)
    const nextId = Math.max(...newTests.map((test) => test.id), 0) + 1
    newTests.push({
      id: nextId,
      ...testResult,
    })
    localStorage.setItem('testResults', JSON.stringify(newTests))
    return newTests
  }

  /** Получить список результатов тестов */
  static getTestResults(): Promise<TestResult[]> {
    return new Promise<TestResult[]>((resolve) => {
      setTimeout(() => {
        const testResults = localStorage.getItem('testResults')
        const tests: TestResult[] = testResults ? JSON.parse(testResults) : []
        resolve(tests)
      }, sampleDelay)
    })
  }

  /** Получить результат теста по id */
  static async getTestResultById(id: number): Promise<TestResult> {
    return new Promise<TestResult>((resolve, reject) => {
      setTimeout(() => {
        const testResults = localStorage.getItem('testResults')
        const tests: TestResult[] = testResults ? JSON.parse(testResults) : []
        const test = tests.find((test) => test.id === id)
        if (test) {
          resolve(test as TestResult)
        } else {
          reject(new Error('Test not found'))
        }
      }, sampleDelay)
    })
  }

  /** Получить результаты теста по id теста */
  static async getTestResultByTestId(testId: number): Promise<TestResult> {
    return new Promise<TestResult>((resolve, reject) => {
      setTimeout(() => {
        const testResults = localStorage.getItem('testResults')
        const tests: TestResult[] = testResults ? JSON.parse(testResults) : []
        const testResultByTestId = tests.find((test) => test.data.id === testId)
        if (testResultByTestId) {
          resolve(testResultByTestId)
        } else {
          reject(new Error('Test not found'))
        }
      }, sampleDelay)
    })
  }
}

export default TestService
