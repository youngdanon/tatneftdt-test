import { TestResult } from '@/types/TestResults'

export type SaveTestResultsDto = Omit<TestResult, 'id'>
