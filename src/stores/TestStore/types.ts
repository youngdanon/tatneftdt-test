import { QuestionStoreExportType } from '../QuestionStore/types'

export type TestStoreExportType = {
  id: number
  /** Название теста */
  name: string
  /** Имя пользователя */
  userName: string
  /** Вопросы теста */
  questions: QuestionStoreExportType[]
  /** Баллы пользователя */
  userScore: number
  /** Решен ли тест */
  isUserSolved: boolean
  /** Максимальное кол-во баллов за тест */
  maxScore: number
}
