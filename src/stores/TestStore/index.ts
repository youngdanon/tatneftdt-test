import { action, computed, makeObservable, observable, runInAction } from 'mobx'
import { ScoresByTypes } from '@/types/Test'
import TestService from '@/services/testsService'
import QuestionStore from '../QuestionStore'
import ExportableStore from '../BaseStores/DumpableStore'
import { TestStoreExportType } from './types'

class TestStore extends ExportableStore<TestStoreExportType> {
  /** Id теста */
  @observable id: number = 0
  /** Название теста */
  @observable name: string = ''
  /** Баллы за различные типы вопросов */
  @observable scoresByTypes: ScoresByTypes = {
    single: 0,
    multiple: 0,
  }
  /** Вопросы теста */
  @observable questions: QuestionStore[] = []

  /** Текущий индекс вопроса */
  @observable currentQuestionIndex: number = 0

  /** Решил ли пользователь тест */
  @observable isUserSolved: boolean = false

  /** Время начала теста */
  @observable testStart: string = ''

  /** Время завершения теста */
  @observable testEnd: string = ''

  /** Имя пользователя */
  @observable userName: string = ''

  constructor() {
    super()
    makeObservable(this)
  }

  /** Кол-во вопросов */
  @computed get totalQuestionsCount(): number {
    return this.questions.length
  }

  @computed get userPassedQuestionsCount(): number {
    return this.questions.filter((question) => question.isUserAnswered).length
  }

  /** Прогресс по тесту в процентах */
  @computed get progress(): number {
    return this.totalQuestionsCount ? this.currentQuestionIndex / this.totalQuestionsCount : 0
  }

  /** Текущий вопрос */
  @computed get currentQuestion(): QuestionStore | undefined {
    return this.questions[this.currentQuestionIndex]
  }

  /** Является ли текущий вопрос последним */
  @computed get isCurrentQuestionLast(): boolean {
    return this.currentQuestionIndex === this.totalQuestionsCount - 1
  }

  /** Установить имя пользователя */
  @action setUserName(value: string): void {
    this.userName = value
  }

  /** Получить тест по id */
  @action async fetch(id: number): Promise<void> {
    this.runWithStateControl(async () => {
      const test = await TestService.getTestById(id)
      runInAction(() => {
        this.id = test.id
        this.name = test.name
        this.scoresByTypes = test.scoresByTypes
        this.questions = test.questions.map(
          (question) => new QuestionStore(question, this.scoresByTypes[question.type])
        )
      })
    })
  }

  /** Начать тест */
  @action startTest(): void {
    this.testStart = new Date().toISOString()
  }

  /** Установить текущий индекс вопроса */
  @action setCurrentQuestionIndex(value: number) {
    this.currentQuestionIndex = value
  }

  /** Следующий вопрос */
  @action nextQuestion(): void {
    if (this.currentQuestionIndex < this.totalQuestionsCount - 1) {
      this.currentQuestionIndex++
    }
  }

  /** Завершить тест */
  @action finishTest(): void {
    this.isUserSolved = true
    this.testEnd = new Date().toISOString()
    const dataToSave = this.export()
    TestService.saveTestResults({
      testStart: this.testStart,
      testEnd: this.testEnd,
      data: dataToSave,
    })
  }

  export(): TestStoreExportType {
    const questionsToExport = this.questions.map((question) => question.export())
    return {
      id: this.id,
      name: this.name,
      questions: questionsToExport,
      userScore: questionsToExport.reduce((acc, question) => acc + question.userScore, 0),
      isUserSolved: this.isUserSolved,
      maxScore: questionsToExport.reduce((acc, question) => acc + question.maxScore, 0),
      userName: this.userName,
    }
  }

  clearStore(): void {
    super.clearStore()
    this.id = 0
    this.name = ''
    this.scoresByTypes = {
      single: 0,
      multiple: 0,
    }
    this.questions = []
    this.currentQuestionIndex = 0
    this.isUserSolved = false
    this.testStart = ''
    this.testEnd = ''
  }
}

export default TestStore
