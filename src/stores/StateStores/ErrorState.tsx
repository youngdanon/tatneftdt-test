import BaseState from './BaseState'
import Status from '@/types/Status'

/** Состояние стора с ошибкой */
class ErrorState extends BaseState {
  constructor(error?: unknown) {
    super(Status.Error, (error as Error)?.message ?? 'Произошла непредвиденная ошибка')
  }
}

export default ErrorState
