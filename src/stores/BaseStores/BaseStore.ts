import { action, computed, makeObservable, observable, runInAction } from 'mobx'
import { BaseState, ErrorState, FetchingState, SuccessState } from '../StateStores'

abstract class BaseStore {
  /** Состояние стора */
  @observable protected _state: BaseState = new BaseState()

  @computed get state(): BaseState {
    return this._state
  }

  /** Состояние стора */
  constructor() {
    makeObservable(this)
  }

  /** Выполнение кастомного метода с контролированием стейта */
  @action protected async runWithStateControl<T = void>(
    func: () => Promise<T>
  ): Promise<T | undefined> {
    this._state = new FetchingState()
    try {
      const result = await func()
      runInAction(() => {
        this._state = new SuccessState()
      })
      return result
    } catch (e) {
      runInAction(() => {
        this._state = new ErrorState(e)
      })
      return undefined
    }
  }

  /** Очистить стор */
  @action clearStore(): void {
    this._state = new BaseState()
  }

  /** Очистка ошибки */
  @action clearError(): void {
    this._state = new SuccessState()
  }
}

export default BaseStore
