import { makeObservable } from 'mobx'
import BaseStore from './BaseStore'

abstract class ExportableStore<ExportType> extends BaseStore {
  /** Состояние стора */
  constructor() {
    super()
    makeObservable(this)
  }

  /** Выгрузка данных из стора */
  abstract export(): ExportType
}

export default ExportableStore
