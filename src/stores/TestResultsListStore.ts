import { action, makeObservable, observable, runInAction } from 'mobx'
import BaseStore from './BaseStores/BaseStore'
import TestService from '@/services/testsService'
import { TestResult } from '@/types/TestResults'

class TestResultsListStore extends BaseStore {
  @observable testResults: TestResult[] = []

  constructor() {
    super()
    makeObservable(this)
  }

  @action async fetchTestResults() {
    await this.runWithStateControl(async () => {
      runInAction(async () => {
        this.testResults = await TestService.getTestResults()
      })
    })
  }

  clearStore(): void {
    super.clearStore()
    this.testResults = []
  }
}

export default TestResultsListStore
