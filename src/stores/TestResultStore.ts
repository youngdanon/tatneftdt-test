import { action, computed, makeObservable, observable, runInAction } from 'mobx'
import BaseStore from './BaseStores/BaseStore'
import { TestResult } from '@/types/TestResults'
import TestService from '@/services/testsService'
import { QuestionStoreExportType } from './QuestionStore/types'

class TestResultStore extends BaseStore {
  @observable data: TestResult | null = null

  constructor() {
    super()
    makeObservable(this)
  }

  /** Получить результат теста по id теста */
  @action fetch(testId: number) {
    this.runWithStateControl(async () => {
      runInAction(async () => {
        this.data = await TestService.getTestResultByTestId(testId)
      })
    })
  }

  /** Получить название теста */
  @computed get testName(): string {
    return this.data?.data.name || ''
  }

  /** Кол-во верных ответов пользователя */
  @computed get correctAnswersCount(): number {
    return (
      this.data?.data.questions.filter((question) => question.isUserAnsweredCorrectly).length || 0
    )
  }

  /** Кол-во вопросов всего */
  @computed get totalQuestionsCount(): number {
    return this.data?.data.questions.length || 0
  }

  /** Баллы пользователя */
  @computed get userScore(): string {
    return this.data?.data.userScore?.toFixed(2).replace('.00', '') || '0'
  }

  /** Максимальное кол-во баллов */
  @computed get maxScore(): number {
    return this.data?.data.maxScore || 0
  }

  /** Время, затраченное на тест */
  @computed get timeSpent(): string {
    const startDate = new Date(this.data?.testStart ?? '')
    const endDate = new Date(this.data?.testEnd ?? '')
    const delta = new Date(endDate.getTime() - startDate.getTime())

    return `${delta.getMinutes()} минут ${delta.getSeconds()} секунд`
  }

  /** Получить дату прохождения */
  @computed get passedAt(): string {
    return this.data?.testStart ?? ''
  }

  /** Получить имя проходившего */
  @computed get userName(): string {
    return this.data?.data.userName ?? ''
  }

  /** Получить ответы пользователя на вопрос */
  getUserAnswers(question: QuestionStoreExportType) {
    return question.answers.filter((answer) => question.userAnswers.includes(answer.id))
  }

  /** Получить правильные ответы на вопрос */
  getCorrectAnswers(question: QuestionStoreExportType) {
    return question.answers.filter((answer) => answer.isCorrect)
  }

  clearStore() {
    super.clearStore()
    this.data = null
  }
}

export default TestResultStore
