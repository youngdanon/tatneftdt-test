import { action, makeObservable, observable, runInAction } from 'mobx'
import BaseStore from './BaseStores/BaseStore'
import { Test } from '@/types/Test'
import TestService from '@/services/testsService'

class TestsListStore extends BaseStore {
  /** Список тестов */
  @observable tests: Test[] = []
  @observable solvedTestsIds: number[] = []

  constructor() {
    super()
    makeObservable(this)
  }

  /** Получить список тестов */
  @action async fetchTests(): Promise<void> {
    await this.runWithStateControl(async () => {
      runInAction(async () => {
        this.tests = await TestService.getTestsList()
      })
    })
  }

  /** Получить список тестов по названию */
  @action async findTestsByName(name: string): Promise<void> {
    await this.runWithStateControl(async () => {
      runInAction(async () => {
        this.tests = await TestService.findTestsByName(name)
      })
    })
  }

  /** Получить список id решенных тестов */
  @action async fetchSolvedTestsIds() {
    this.runWithStateControl(async () => {
      runInAction(async () => {
        const testResults = await TestService.getTestResults()
        this.solvedTestsIds = testResults.map((testResult) => testResult.data.id)
      })
    })
  }

  clearStore(): void {
    super.clearStore()
    this.tests = []
    this.solvedTestsIds = []
  }
}

export default TestsListStore
