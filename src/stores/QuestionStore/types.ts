import { Answer, QuestionType } from '@/types/Test'

export type QuestionStoreExportType = {
  id: number
  /** Текст вопроса */
  question: string
  /** Тип вопроса */
  type: QuestionType
  /** Ответы на вопрос */
  answers: Answer[]
  /** Максимальное кол-во баллов за вопрос */
  maxScore: number
  /** Баллы пользователя */
  userScore: number
  /** Ответы пользователя */
  userAnswers: number[]
  /** Ответил ли пользователь на вопрос */
  isUserAnswered: boolean
  /** Правильно ли ответил пользователь */
  isUserAnsweredCorrectly: boolean
}
