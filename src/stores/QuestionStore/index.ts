import { COLORS } from '@/theme/colors'
import { Answer, Question, QuestionType } from '@/types/Test'
import { action, computed, makeObservable, observable } from 'mobx'
import ExportableStore from '../BaseStores/DumpableStore'
import { QuestionStoreExportType } from './types'

class QuestionStore extends ExportableStore<QuestionStoreExportType> {
  /** id вопроса */
  @observable id: number = 0
  /** Текст вопроса */
  @observable private _question: string = ''
  /** Тип вопроса */
  @observable type: QuestionType = QuestionType.Single
  /** Ответы на вопрос */
  @observable private _answers: Answer[] = []
  /** Максимальное кол-во баллов за вопрос */
  @observable maxScore: number = 0

  /** Ответы пользователя */
  @observable userAnswers: number[] = []
  /** Сдал ли пользователь вопрос на проверку */
  @observable isUserAnswered: boolean = false

  /**
   * @param question - объект вопроса
   * @param scoreForQuestion - Максимальное кол-во баллов за вопрос
   */
  constructor(question: Question, scoreForQuestion: number) {
    super()
    makeObservable(this)
    this.id = question.id
    this._question = question.question
    this.type = question.type
    this._answers = question.answers
    this.maxScore = scoreForQuestion
  }

  /** Варианты ответа в случайном порядке */
  @computed get shuffledAnswers(): Answer[] {
    return this._answers.slice().sort(() => Math.random() - 0.5)
  }

  /** Может ли пользователь ответить на вопрос */
  @computed get canUserAnswer(): boolean {
    return this.userAnswers.length > 0 && !this.isUserAnswered
  }

  /** Правильно ли ответил пользователь */
  @computed get isUserAnsweredCorrectly(): boolean {
    if (this.type === QuestionType.Single) {
      const correctAnswer = this._answers.find((answer) => answer.isCorrect)
      if (correctAnswer) {
        return correctAnswer.id === this.userAnswers[0]
      }
    }

    if (this.type === QuestionType.Multiple) {
      const correctAnswers = this._answers
        .filter((answer) => answer.isCorrect)
        .map((answer) => answer.id)
      return (
        this.userAnswers.length === correctAnswers.length &&
        this.userAnswers.every((answer) => correctAnswers.includes(answer))
      )
    }

    return false
  }

  /** Кол-во баллов за ответ
   * @description Если тип вопроса - single, то возвращается 0 или maxScore.
   * @description Если тип вопроса - multiple, то возвращается 0, maxScore или maxScore / кол-во правильных ответов
   */
  @computed get score(): number {
    return this.isUserAnsweredCorrectly ? this.maxScore : 0
  }

  @computed get questionName(): string {
    return this._question
  }

  @computed get questionHintColor(): string {
    if (this.isUserAnswered) {
      return this.isUserAnsweredCorrectly ? COLORS.success.main : COLORS.error.main
    }
    return COLORS.custom.slate[400]
  }

  @computed get questionHint(): string {
    if (this.isUserAnswered) {
      return this.isUserAnsweredCorrectly ? 'Верно' : 'Неверно'
    }
    if (this.type === QuestionType.Single) {
      return 'Выберите один вариант ответа'
    }
    if (this.type === QuestionType.Multiple) {
      return 'Выберите несколько вариантов ответа'
    }
    return ''
  }

  @action setUserAnswers(value: number[]): void {
    this.userAnswers = value
  }

  @action answerQuestion(): void {
    this.isUserAnswered = true
  }

  export(): QuestionStoreExportType {
    return {
      id: this.id,
      question: this._question,
      type: this.type,
      answers: this._answers,
      maxScore: this.maxScore,
      userScore: this.score,
      userAnswers: this.userAnswers,
      isUserAnswered: this.isUserAnswered,
      isUserAnsweredCorrectly: this.isUserAnsweredCorrectly,
    }
  }
}

export default QuestionStore
