import { usePortalTheme } from '@/hooks/usePortalTheme'
import { CssBaseline, ThemeProvider } from '@mui/material'
import { RootRoutes } from './routes'

function App() {
  const theme = usePortalTheme()

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <RootRoutes />
    </ThemeProvider>
  )
}

export default App
